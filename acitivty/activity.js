//import the http module using the required directive
const http = require('http')

//create a variable port and assign it with the value of 3000
const port = 3000

//create a server using the createServer method that will listen in to the port provided above
const server = http.createServer(function(request, response){

	//create a condition that when the login route is accessed, it will print a message to the user that they are in the login page
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Welcome to the login page.')
	} 

	else if (request.url == '/register'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Im sorry the page you are looking for cannot be found.' )
	} 
	//create a condition for any other routes that will return an error message
	else {
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Page not available')
	}
}) 

server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)